#include <iostream>
#include "Timer.h"

using namespace std;

const int N = 1024;

typedef float real;

real A[N][N];
real B[N][N];
real C1[N][N];
real C2[N][N];


void SimpleMatrixMul(real a[N][N], real b[N][N], real c[N][N])
{
	for(int i=0;i<N;i++)
		for(int j=0;j<N;j++)
			for(int k=0;k<N;k++)
				c[i][j] += a[i][k]*b[k][j];
}

void SimpleMatrixMul2(real a[N][N], real b[N][N], real c[N][N])
{
  for(int i=0;i<N;i++)
    for(int j=0;j<N;j++)
      for(int k=0;k<N;k++)
        c[i][j] += a[i][k]*b[k][j];
}

void ZeroMatrix(real m[N][N])
{
	for(int i=0;i<N;i++)
		for(int j=0;j<N;j++)
			m[i][j]=0;
}


bool VerifyResult(real a[N][N], real b[N][N])
{
	for(int i=0;i<N;i++)
		for(int j=0;j<N;j++)
			if(abs(a[i][j] - b[i][j]) > 1e-5f)
				return false;
	return true;
}

void InitRandomMatrix(real a[N][N])
{
	for(int i=0;i<N;i++)
		for(int j=0;j<N;j++)
      a[i][j] = (rand()%65536)/65535.0f;
}


int main()
{
	InitRandomMatrix(A);
	InitRandomMatrix(B);
  ZeroMatrix(C1);
  ZeroMatrix(C2);
  
  Timer timer(true);

	SimpleMatrixMul(A,B,C1);
	

  float t1 = timer.getElapsed();
  timer.start();
  cout << "time1 = " << t1 << std::endl;

  SimpleMatrixMul2(A,B,C2);
	
  float t2 = timer.getElapsed();

  cout << "time2 = " << t2 << std::endl;

	cout << "result: "       << VerifyResult(C2,C1) << endl;
	cout << "acceleration: " << (float)(t1)/(float(t2)) << endl;

	return 0;
}










































/*

// g++ -O3 -mavx main.cpp Timer.cpp -fopenmp -o zout // well, man not be correct actually due to concurrent +=, but ok for perf testing

void SimpleMatrixMul2(real a[N][N], real b[N][N], real c[N][N])
{
  #pragma omp parallel for
  for(int i=0;i<N;i++)
    for(int k=0;k<N;k++)
      for(int j=0;j<N;j++)
        c[i][j] += a[i][k]*b[k][j];
}

*/
